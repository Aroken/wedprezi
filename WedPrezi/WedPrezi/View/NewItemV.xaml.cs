﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WedPrezi.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WedPrezi.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class NewItemV : ContentPage
	{
		public NewItemV()
		{
			InitializeComponent();
			BindingContext = new NewItemVM();
			
		}
	}
}