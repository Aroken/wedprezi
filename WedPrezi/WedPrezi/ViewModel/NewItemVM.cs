﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Input;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using WedPrezi.Annotations;
using WedPrezi.Model;
using Xamarin.Forms;

namespace WedPrezi.ViewModel
{
	class NewItemVM: INotifyPropertyChanged
	{
		private string _pickedImg;
		public Team Team { get; set; }
		public ICommand PickImageCommand { get; set; }

		public string PickedImg
		{
			get => _pickedImg;
			set
			{
				if (value == _pickedImg) return;
				_pickedImg = value;
				OnPropertyChanged();
			}
		}

		public NewItemVM()
		{
			Team = new Team(){SwitchTest = new SwitchTest()};
			PickImageCommand = new Command(PickImage);
		}

		private async void PickImage()
		{
			//PermissionStatus permissionStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.MediaLibrary);
			//Dictionary<Permission, PermissionStatus> response = null;
			//if (permissionStatus != PermissionStatus.Granted)
			//{
			//	response = await CrossPermissions.Current.RequestPermissionsAsync(Permission.MediaLibrary);
			//	if (response[Permission.MediaLibrary] == PermissionStatus.Granted)
			//	{
			//		if (CrossMedia.Current.IsPickPhotoSupported)
			//		{
			//			MediaFile photo = await CrossMedia.Current.PickPhotoAsync();
			//			PickedImg = photo.Path;
			//		}
			//	}
			//}
			//else
			//{
				
			//}

			if (CrossMedia.Current.IsPickPhotoSupported)
			{
				MediaFile photo = await CrossMedia.Current.PickPhotoAsync();
				PickedImg = photo.Path;
			}

		}

		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
