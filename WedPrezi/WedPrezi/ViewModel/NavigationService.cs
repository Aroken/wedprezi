﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using WedPrezi.View;
using Xamarin.Forms;

namespace WedPrezi.ViewModel
{
	class NavigationService
	{
		private static NavigationService _service;
		private static INavigation _appNavigation;
		private static IDictionary<string, Type> _pages;
		private NavigationService()
		{
			_pages = new Dictionary<string, Type>();
		}

		public static NavigationService GetNavigationService()
		{
			if (_service == null)
			{
				return new NavigationService();
			}
			
			return _service;
		}

		public static void SetAppNavigation(INavigation nav)
		{
			GetNavigationService();
			if (_appNavigation == null) _appNavigation = nav;
		}

		public static void AddPage(string name, Type pageType)
		{
			GetNavigationService();
			if (!_pages.Keys.Contains(name)) _pages.Add(name, pageType);
		}

		public static async void NavToPage(string name)
		{
			if (_pages.Keys.Contains(name))
			{
				ConstructorInfo constructor = _pages[name].GetTypeInfo().DeclaredConstructors.FirstOrDefault(c => !c.GetParameters().Any());
				if (constructor != null)
				{
					
					await _appNavigation.PushAsync(constructor.Invoke(new object[0]) as Page, true);
				}
			}
			
		}

	}
}
