﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WedPrezi.View;
using WedPrezi.ViewModel;
using Xamarin.Forms;

namespace WedPrezi
{
	// Learn more about making custom code visible in the Xamarin.Forms previewer
	// by visiting https://aka.ms/xamarinforms-previewer
	[DesignTimeVisible(false)]
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
			NavigationPage.SetHasNavigationBar(this, false);
			NavigationService.SetAppNavigation(Navigation);
			NavigationService.AddPage("MainPage", typeof(MainPage));
			NavigationService.AddPage("NewItemV", typeof(NewItemV));
		}
	}
}
