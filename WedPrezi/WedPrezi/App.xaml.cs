﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;


namespace WedPrezi
{
	public partial class App : Application
	{
		public App()
		{
			InitializeComponent();

			this.MainPage = GetMainPage();
			
			BindingContext = new MainVm();
			
		}
		public static Page GetMainPage()
		{
			return new NavigationPage(new MainPage());
		}

		protected override void OnStart()
		{
			// Handle when your app starts
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}
	}


}
