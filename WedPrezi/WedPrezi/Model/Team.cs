﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using WedPrezi.Annotations;

namespace WedPrezi.Model
{
	public class Team : INotifyPropertyChanged
	{
		private string _name;
		private int _percentage;


		public string Name
		{
			get => _name;
			set
			{
				if (value == _name) return;
				_name = value;
				OnPropertyChanged();
			}
		}

		public int Percentage
		{
			get => _percentage;
			set
			{
				if (value == _percentage) return;
				_percentage = value;
				OnPropertyChanged();
			}
		}

		public string Logo { get; set; }

		public SwitchTest SwitchTest { get; set; }


		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
