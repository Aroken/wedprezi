﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using WedPrezi.Annotations;
using WedPrezi.Model;
using WedPrezi.ViewModel;
using Xamarin.Forms;

namespace WedPrezi
{
	public class MainVm : INotifyPropertyChanged
	{
		private int _widthwed;
		public BindingList<Team> Teams { get; set; }

		public bool SwitchCurr
		{
			get => _switchCurr;
			set
			{
				if (value == _switchCurr) return;
				_switchCurr = value;
				OnPropertyChanged();
			}
		}

		public Slider Slider
		{
			get => _slider;
			set
			{
				if (Equals(value, _slider)) return;
				_slider = value;
				OnPropertyChanged();
			}
		}

		public int Widthwed
		{
			get => _widthwed;
			set
			{
				if (value == _widthwed) return;
				_widthwed = value;
				OnPropertyChanged();
			}
		}

		public Team SelectedTeam
		{
			get => _selectedTeam;
			set
			{
				if (Equals(value, _selectedTeam)) return;
				_selectedTeam = value;
				OnPropertyChanged();
			}
		}

		public ICommand SelectedImageCommand { get; set; }
		public MainVm()
		{
			SelectedImageCommand = new Command(() =>
			{
				NavigationService.NavToPage("NewItemV");
			});
			Slider = new Slider(){Min = 0, Max = 200, Curr = 0};
			Widthwed = 0;
			
			Teams = new BindingList<Team>()
			{
				new Team(){Name = "WED", Percentage = 0, Logo = "Img/testwed.png", SwitchTest = new SwitchTest(){Switch = false}},
				new Team(){Name = "CONTI", Percentage = 0, Logo = "Img/conti.png", SwitchTest = new SwitchTest(){Switch = false}}
			};
			
			Task newTask = new Task(RunTest);
			newTask.Start();
		}
		delegate void ChangeWithWed();
		ChangeWithWed changeWithWed = () => { };
		private Slider _slider;
		private bool _switchCurr;
		private Team _selectedTeam;

		private void RunTest()
		{
			int count = 0;
			while (true)
			{
				if (SelectedTeam == null)
				{
					continue;
				}

				if (SelectedTeam.SwitchTest.Switch)
					Widthwed = Slider.Curr;
				else
				{
					count = count < 0 || count > 200 ? 0 : count;
					Widthwed = count;
				}
					
				Thread.Sleep(20);
				if (count == 200)
				{
					changeWithWed = () => count--;
				}

				if (Widthwed == 0)
				{
					changeWithWed = () => count++;
				}

				SelectedTeam.Percentage = Widthwed;
				
				changeWithWed.Invoke();
			}
		}

		

		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}

	

	public class Slider : INotifyPropertyChanged
	{
		private int _curr;
		public int Max { get; set; }
		public int Min { get; set; }

		public int Curr
		{
			get => _curr;
			set
			{
				if (value == _curr) return;
				_curr = value;
				OnPropertyChanged();
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}

	public class SwitchTest : INotifyPropertyChanged
	{
		private bool _switch;

		public bool Switch
		{
			get => _switch;
			set
			{
				if (value == _switch) return;
				_switch = value;
				OnPropertyChanged();
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

		}
	}
}
